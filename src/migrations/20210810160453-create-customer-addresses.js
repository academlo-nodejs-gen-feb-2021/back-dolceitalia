'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('customer_addresses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      line_1: {
        type: Sequelize.STRING,
        allowNull: false
      },
      line_2: {
        type: Sequelize.STRING
      },
      line_3: {
        type: Sequelize.STRING
      },
      line_4: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING,
        allowNull: false
      },
      zip_postcode: {
        type: Sequelize.STRING,
        allowNull: false
      },
      state_province: {
        type: Sequelize.STRING,
        allowNull: false
      },
      country: {
        type: Sequelize.STRING,
        allowNull: false
      },
      location: {
        type: Sequelize.GEOMETRY('POINT', 4326)
      },
      other_details: {
        type: Sequelize.TEXT
      },
      user_uuid: {
        type: Sequelize.UUID
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('customer_addresses');
  }
};