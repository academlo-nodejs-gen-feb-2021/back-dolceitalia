'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CustomerAddresses extends Model {
    static associate(models) {
      // define association here
    }
  };
  CustomerAddresses.init({
    line_1: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    line_2: DataTypes.STRING,
    line_3: DataTypes.STRING,
    line_4: DataTypes.STRING,
    city: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    zip_postcode: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    state_province: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    country: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    location: DataTypes.GEOMETRY,
    other_details: DataTypes.TEXT,
    user_uuid: DataTypes.UUID
  }, {
    sequelize,
    modelName: 'CustomerAddresses',
    tableName: 'customer_addresses'
  });
  return CustomerAddresses;
};