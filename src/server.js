//Obtener la configuración de las variables de entorno .env
require('dotenv').config();

const app = require('./app');

/* Si no se encuentra definida la variable PORT en el archivo .env -> asignar el puerto 8080 por defecto */
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
    console.log(`El servidor está escuchando sobre el puerto ${PORT}`);
});