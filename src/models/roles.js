"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Roles extends Model {
        static associate(models) {
            Roles.hasMany(models.Users, {
                foreignKey: 'role_id',
                as: "role_detail"
            });
        }
    }
    Roles.init(
        {
            name: {
                type: DataTypes.STRING,
                validate: { notEmpty: true },
            },
            description: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Roles",
            tableName: "roles",
        }
    );
    return Roles;
};
