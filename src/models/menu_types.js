'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MenuTypes extends Model {
    static associate(models) {
      // define association here
    }
  };
  MenuTypes.init({
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  }, {
    sequelize,
    modelName: 'MenuTypes',
    tableName: 'menu_types'
  });
  return MenuTypes;
};