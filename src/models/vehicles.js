"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Vehicles extends Model {
        static associate(models) {
            // define association here
        }
    }
    Vehicles.init(
        {
            registration_place: {
                type: DataTypes.STRING,
                validate: {
                    notEmpty: true,
                },
            },
            licence_number: { type: DataTypes.STRING },
            brand: { type: DataTypes.STRING },
            model: { type: DataTypes.STRING },
        },
        {
            sequelize,
            modelName: "Vehicles",
            tableName: "vehicles"
        }
    );
    return Vehicles;
};
