'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      Users.belongsTo(models.Roles, {
        foreignKey: 'role_id',
        as: "role_detail"
      });
    }
  };
  Users.init({
    uuid: {
      type: DataTypes.UUID,
      primaryKey: true
    },
    firstname: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    lastname: { 
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    email: { 
      type: DataTypes.STRING,
      validate: {
        isEmail: true
      }
    },
    telephone: DataTypes.STRING,
    password: {
      type: DataTypes.STRING,
      validate: {
        isPassword(value){
          if(value.length > 8){
            const customError = new Error('La contraseña debe tener al menos 8 caracteres');
            customError.name = "InvalidPassword";
            throw customError;
          }
        }
      }
    },
    verified: DataTypes.BOOLEAN,
    active: DataTypes.BOOLEAN,
    role_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Users',
    tableName: 'users'
  });
  return Users;
};