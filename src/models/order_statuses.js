'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OrderStatuses extends Model {
    static associate(models) {
      // define association here
    }
  };
  OrderStatuses.init({
    title: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    }
  }, {
    sequelize,
    modelName: 'OrderStatuses',
    tableName: 'order_statuses'
  });
  return OrderStatuses;
};