const express = require('express');
//Para poder obtener información de las peticiones que llegan al servidor
const morgan = require('morgan');

const {Users, Roles} = require("./models");

const app = express();

const VERSION = process.env.VERSION;

app.use(morgan('dev'));

app.get(`/api/${VERSION}/`, ({res}) => {
    res.json({
        api: "API RESTAURANT",
        version: VERSION
    });
});

app.get(`/api/${VERSION}/users`, async({res}) => {
    const users = await Users.findAll({
        attributes: ["uuid", "firstname", "lastname"],
        include: [
            {
                model: Roles,
                as: "role_detail",
                attributes: ["id", "name"]
            }
        ]
    });
    res.json(users);
});

module.exports = app;