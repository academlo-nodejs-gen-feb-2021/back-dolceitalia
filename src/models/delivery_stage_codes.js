"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class DeliveryStageCodes extends Model {
        static associate(models) {
            // define association here
        }
    }
    DeliveryStageCodes.init(
        {
            title: {
                type: DataTypes.STRING,
                validate: {
                    notEmpty: true,
                },
            },
            color: {
                type: DataTypes.STRING,
                validate: {
                    notEmpty: true,
                },
            },
        },
        {
            sequelize,
            modelName: "DeliveryStageCodes",
            tableName: "delivery_stage_codes",
        }
    );
    return DeliveryStageCodes;
};
